/*

    Minimum Profit 5.x
    A Programmer's Text Editor

    Version Control System support

    Copyright (C) 1991-2012 Angel Ortega <angel@triptico.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

    http://www.triptico.com

*/

/** data **/

mp.vcs = {
    git: {
        id:     'Git',
        check:  [ '.git', '../.git', '../../.git' ],
        opts:   {
            'git diff'          => NULL,
            'git status'        => NULL,
            'git log'           => NULL,
            "git diff '%s'"     => NULL,
            "git log '%s'"      => NULL,
            "git add '%s'"      => NULL,
            "git blame '%s'"    => NULL,
            "git checkout '%s'" => NULL,
            "git stash"         => NULL,
            "git stash apply"   => NULL,
            "git push"          => NULL,
            "git pull"          => NULL,
            "git commit -a"     => {
                pre:    sub (doc) {
                    mp.vcs_commit_on_close(doc, "git commit -a -F '%s'");
                }
            }
        }
    },
    svn: {
        id:     'SVN',
        check:  [ '.svn' ],
        opts:   {
            'svn diff'          => NULL,
            'svn status'        => NULL,
            'svn log'           => NULL,
            "svn diff '%s'"     => NULL,
            "svn log '%s'"      => NULL,
            "svn add '%s'"      => NULL,
            "svn annotate '%s'" => NULL,
            "svn update"        => NULL,
            "svn commit"        => {
                pre:    sub (doc) {
                    mp.vcs_commit_on_close(doc, "svn commit -F '%s'");
                }
            }
        }
    },
    mercurial: {
        id:     'Mercurial',
        check:  [ '.hg', '../.hg', '../../.hg' ],
        opts:   {
            'hg diff'          => NULL,
            'hg status'        => NULL,
            'hg log'           => NULL,
            "hg diff '%s'"     => NULL,
            "hg log '%s'"      => NULL,
            "hg add '%s'"      => NULL,
            "hg push"          => NULL,
            "hg pull"          => NULL,
            "hg annotate '%s'" => NULL,
            "hg commit -A"     => {
                pre:    sub (doc) {
                    mp.vcs_commit_on_close(doc, "hg commit -A -l '%s'");
                }
            }
        }
    }
};

/** editor actions **/

mp.actions['vcs'] = sub (d) {
    local v = NULL;

    foreach (k, keys(mp.vcs)) {
        local n = mp.vcs[k];

        foreach (d, n.check) {
            if (stat(d) != NULL) {
                v = n;
                break;
            }
        }

        if (v != NULL)
            break;
    }

    if (v == NULL)
        mp.alert(L("This directory is not under a supported version control system"));
    else {
        local l = sort(keys(v.opts));

        local t = mp.form(
            [
                {
                    label:  sprintf(L("Available %s commands"), v.id),
                    type:   'list',
                    list:   l,
                    value:  mp.vcs_target
                }
            ]
        );

        if (t != NULL) {
            mp.vcs_target = t[0];

            local cmd = sprintf(l[mp.vcs_target], d.name);

            /* if a 'pre' hook exists, run it and get the
               cmd to be executed (NULL if none) */
            if (v.opts[cmd].pre) {
                cmd = v.opts[cmd].pre(d);
            }

            if (cmd != NULL) {
                local log = mp.open(sprintf(L("<%s output>"), cmd));

                log.txt.lines   = [ '' ];
                log.txt.y       = 0;

                if (v.opts[cmd] == NULL) {
                    mp.actions.exec_command(log, cmd);
                }

                mp.detect_syntax(log);
                mp.move_bof(log);
                log.txt.mod     = 0;
                log.undo        = [];
                log.read_only   = 1;
            }
        }
    }
};

/** default key bindings **/

mp.keycodes['ctrl-p'] = 'vcs';

/** action descriptions **/

mp.keycodes['vcs'] = LL("Version Control...");

/** code **/

sub mp.vcs_commit_on_close(doc, cmd)
/* on_close handler for commit message documents */
{
    /* create a commit message */
    local ci = mp.open('.COMMIT_MESSAGE');

    ci.commit_cmd = cmd;

    /* and add an on_close handler */
    push(ci.on_close,
        sub (doc) {
            local ret = 1;

            if (stat(doc.name)) {
                doc.txt.lines   = [ '' ];
                doc.txt.y       = 0;
                mp.actions.exec_command(doc, sprintf(doc.commit_cmd, doc.name));

                mp.detect_syntax(doc);
                mp.move_bof(doc);
                doc.txt.mod     = 0;
                doc.undo        = [];
                doc.read_only   = 1;

                doc.name = sprintf(L("<%s output>"), doc.commit_cmd);

                /* reject closing (file has been modified) */
                ret = 0;
            }

            unlink('.COMMIT_MESSAGE');

            /* delete this on_close handler */
            pop(doc.on_close);

            return ret;
        }
    );

    return NULL;
}
